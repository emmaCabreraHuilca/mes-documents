# CABRERA HUILCA Emma

## Informations

- Née le 16/09/1999
- 21, rue Charles Paradinas - 92110 - Clichy-la-Garenne
- 07 81 52 13 78
- emmacabrera99@gmail.com

## Compétences 

### Langues

- Français 
- Anglais 

### Informatiques

- World
- Excel
- Power Point

## Formations

### 2017-2018 - L1 de Licence Arts Plastiques
- _Université Paris VIII, Saint-Denis, 93_

### 2017 - Baccalauréat Scientifique - Spécialisé Science de la Vie et de la Terre
- _Lycée Newton - Clichy-la-Garenne, 92_

## Expériences Professionnelles 

### 07/2019 - 08/2019 CDD - Agent Administratif
- _HAD AP-HP soins Adultes Hôpital Louis-Mourier, Colombes, 92_

### 09/2018 - 05/2019 - Mission de Service Civique - Participer à l'améliration des conditions d'acceuil et d'accompagnement des patients et de leurs proches dans le cadre de l'hospitalisation à domicile
- _HAD AP-HP soins Adultes Hôpital Paul Brousse, Villejuif, 94_
- _HAD AP-HP soins Pédiatriques Hôpital Bîcêtre, Le Kremlin-Bicëtre, 94_

## Centre d'intêrets

### Jeux Vidéos 
### Lectures 
### L'Histoire  






 