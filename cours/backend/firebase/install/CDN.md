# CDN

## Présentation

Certaines parties d'un site sont dynamiques et d'autres sont statiques:
- Statique : HTML, CSS, JavaScript (images, texte, vidéos) = contenu = ressources
- Dynamique : bases de données 

Les parties dynamiques du site vont génerer des datas qui vont être envoyés dans les parties statiques.

### Fonctionnement

Pour rendre accesible un site à un maximum de personnes on vas avec un ensemble d'ordinateur situé à travers le monde envoyer des copies des fichiés statiques du site.
> CDN (Content Delivery Network) copier la même ressource pour le mettre a disposition à travers le monde 

 
## Déployer un site sur un CDN
 
En 2018 le meilleur CDN au monde était celui de Google = Firebase

### Sur Firebase

Avant de commencer il faut s'incrire et rentrer certaines informations (site gratuit):
 1. Ajouter un projet
 2. Pas besoin activer 
 3. Hosting => CDN => envoyé dans le monde entier de maniere securiser SSL
 
### Sur Webstorm 

L'installation pour mac est différente, il y a plusieurs problèmes à régler directement sur l'ordinateur Mac. 

1. Copier et coller dans la console de Webstorm le fichié firebase
2. Copier et coller firebase login et **init hosting**
3. Choisir un projet firebase

#### Organiser
 
4. Créer un dossier Public et déplacer tout les dossiers dedans
5. Couper et coller le fichié Home/index à la racine du dossier
- Ok : si il s'agit du dossier Public
- Non : pour single page app (Angular ou React)
- Non : pour ne pas écraser le dossier

Pour finir on rajoute Firebase deploy à la console pour envoyer le site sur internet, un lien URL vers la page internet est donné il faudra la récupérer.

#### Important

Des dossier et des fichiés sont rajoutés automatiquement après avoir déployer son site sur un CDN.
- Les fichiés doivent être git add
- Les dossier doivent être .gitignore
 
 





