# Install Firestore

## 

dans src
- dossier models => fichiés hotel.model.ts / room.model.ts

hotel.model.ts

import {RoomModel} from "./room.model";

export interface HotelModel {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms?: RoomModel[]
}


room.model.ts

export interface RoomModel {
    roomName: string;
    size: number;
    id: number;
}




//--------------------------------------------------------------------


## utilities.ts

import * as admin from 'firebase-admin';
import {HotelModel} from "./models/hotel.model";

let serviceAccount = require('./key.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore();


export function addHostel (hostel: HotelModel): Promise<FirebaseFirestore.DocumentReference> {
    return db
        .collection('hostels')
        .add(hostel)
}



export function removeHostel (id: string): Promise<FirebaseFirestore.WriteResult> {
    return db
        .collection('hostels')
        .doc(id)
        .delete()
}



export function putHostel (id: string, newHostel: HotelModel) {
    return db
        .collection('hostels')
        .doc(id)
        .set(newHostel)
}



export function getHostelById (id: string): Promise<FirebaseFirestore.DocumentSnapshot> {
    return db
        .collection('hostels')
        .doc(id)
        .get()
}


export function patchHostel (id: string, newHostel: HotelModel) {
    return db
        .collection('hostels')
        .doc(id)
        .update (newHostel)
        
}




//--------------------------------------------------------------------


### 2. Functions

Dans index.ts 
- Rajouter => import * as functions from 'firebase-functions';
- Rajouter => export const **test** = **functions**.https.onRequest(app);

Il faut par la suite importé express dans l'application index.ts :

```
import * as express from "express";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
```



## index.ts 

import * as functions from 'firebase-functions';


import * as express from "express";
import {addHostel, getHostelById, patchHostel, putHostel, removeHostel} from "./utilities";
import * as admin from "firebase-admin";


console.log('coucou');


const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));

const db = admin.firestore();


app.get('/', function (req, res) {
    res.send ('ok');
});    


app.post('/hostels', function (req, res) {
    const hostelToCreate = req.body;
    addHostel(hostelToCreate)
        .then(document => document.get())
        .then(createdHostel => res.status(201).send(createdHostel.data()))
        .catch(e => res.status(500).send(e));

});

app.delete('/hostels/:id', function (req, res) {
    const id = req.params.id;
    removeHostel(id)
        .then(result => res.send(result))
        .catch(e => res.status(500).send(e))
});

app.put('/hostels/:id', function (req, res) {
    const hostelToCreated = req.body;
    const id = req.params.id;
    putHostel (id, hostelToCreated)
        .then(hostelCreated => res.send(hostelCreated))
        .catch(e => res.status(500).send(e));

});

app.get('/hostels/:id', function (req, res) {
    const id = req.params.id;
    getHostelById(id)
        .then(hostel =>  res.send(hostel.data()))
        .catch(e => res.send(e));

});

app.patch('/hostels/:id', function (req, res) {
    const hostelToCreated = req.body;
    const id = req.params.id;
    patchHostel (id, hostelToCreated)
        .then(hostelCreated => res.send(hostelCreated))
        .catch(e => res.status(500).send(e));

});




app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});


export const test = functions.https.onRequest(app);


//-------------------------------------------------------------------

index.ts

//Permet de vérifier si la base de donnée fonctionne
/*app.get('/', function (req, res) {

        db.collection('hostels')
            .doc('test')
            .set({tata: 12})
            //promesse
            .then(( ) => res.send('ok'))
            .catch(e => res.status(500).send(e));
});*/