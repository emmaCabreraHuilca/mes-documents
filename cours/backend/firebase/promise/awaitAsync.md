# Await Async


## Ancienne méthode

app.get('/', function (req, res){

    try {
        const data = {};
        db
            .collection('hostels')
            .doc('irOjxREqma200Z2pYhEA')
            .get()
            .then((hotel: DocumentSnapshot) => hotel.data() as HotelModel)
            .then((hotel: HotelModel) => {
                hotel.isValidated = true;
                return hotel
            })
            .then (hotel => res.send(hotel))

    } catch (e) {

    }

});

as => change le type de quelque chose en autre chose (caster?)

## Nouvelle méthode

plus de problème d'asynchronisme grâce aux await 
async  => travailler en asynchrone (sans, les await async ne fonctionne pas)


app.get('/', async function (req, res){

    try {

        const hotelSnapshot = await db
            .collection('hostels')
            .doc('irOjxREqma200Z2pYhEA')
            .get()
        const hotel: HotelModel = hotelSnapshot.data() as HotelModel;

        hotel.isValidated = true;

        console.log(hotel);
        res.send(hotel)


    } catch (e) {
        console.log(e)
    }

});



-----------------------------------------------------------

Si il y a plusieurs hotel : 

app.get('/', async function (req, res){

    try {

        const hotelSnapshot = await db
            .collection('hostels')
            .doc('irOjxREqma200Z2pYhEA')
            .get();
        const hotel: HotelModel = hotelSnapshot.data() as HotelModel;

        const hotelSnapshot2 = await db
            .collection('hostels')
            .doc('Y16JogRX2HV0FFUYeAgz')
            .get();
        const hotel2: HotelModel = hotelSnapshot2.data() as HotelModel;

        hotel.isValidated = true;
        hotel2.isValidated = true;

        console.log(hotel, hotel2);
        res.send([hotel, hotel2]);


    } catch (e) {
        console.log(e)
    }

});



-----------------------------------------------------------

Pour que ça soit plus rapide et plus lisible : => envois les deux promesses en même temps 



app.get('/', async function (req, res){

    try {
        const hotelPromise = db
            .collection('hostels')
            .doc('irOjxREqma200Z2pYhEA')
            .get();

        const hotel2Promise = db
            .collection('hostels')
            .doc('Y16JogRX2HV0FFUYeAgz')
            .get();

       const [hotelSnapshot, hotel2Snapshot] = await Promise.all([
           hotelPromise,
           hotel2Promise
       ]);

       const hotel = hotelSnapshot.data();
       const hotel2 = hotel2Snapshot.data();
       res.send([hotel, hotel2]);

    } catch (e) {
        console.log(e)
    }
});
