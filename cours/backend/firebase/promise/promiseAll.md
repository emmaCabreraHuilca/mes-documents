# Promise All








-----------------------------------------------------------

Récupérer plusierus hotels avec son id :


app.get('/', function (req, res){
    
    const promise1 = db
        .collection('hostels')
        .doc('PdNvO6rpWf8GydSjvb34')
        .get()
        .then((a) => a.data());

    const promise2 = db
        .collection('hostels')
        .doc('Y16JogRX2HV0FFUYeAgz')
        .get()
        .then((a) =>a.data());

    Promise.all([
        promise1,
        promise2
    ])
        .then((result) => res.send(result))
        .catch(e => res.status(500).send(e));

});



------------------------------------------------------------

Créer des données aléatoire 

app.get('/', function (req, res){

    const promiseArray = [];
    let i =0;
    for (; i < 100; i++) {
        promiseArray.push(db.collection('test').add({
            value: i,
        }))

    }

    Promise.all(promiseArray)
        .then(() => res.send(i))
        .catch(e => console.log(e));
});


------------------------------------------------------------

créer des données ou l'on décide de l'id (O à 99)

app.get('/', function (req, res){

    const promiseArray = [];
    let i = 0;
    for (; i < 100; i++) {
        promiseArray.push(db.collection('test').doc(i + '').set({
            value: i,
        }))

    }

    Promise.all(promiseArray)
        .then(() => res.send('document writed:' + i + ''))
        .catch(e => console.log(e));
});

------------------------------------------------------------


Supprime des données 

app.get('/', function (req, res){

    const promiseArray = [];
    let i = 0;
    for (; i < 10; i++) {
        promiseArray.push(db.collection('test').doc(i + '').delete())
    }

    Promise.all(promiseArray)
        .then(() => res.send('document deleted:' + i + ''))
        .catch(e => console.log(e));
});


------------------------------------------------------------

Promise hell => l'enfer des promesses



app.get('/', function (req, res){

    const hostels = [];

    const promise1 = db
        .collection('hostels')
        .doc('irOjxREqma200Z2pYhEA')
        .get()
        .then((a) => hostels.push(a.data()));


    const promise2 = db
        .collection('hostels')
        .doc('Y16JogRX2HV0FFUYeAgz')
        .get()
        .then((a) => hostels.push(a.data()));

    const promise3 = db
        .collection('hostels')
        .doc('PdNvO6rpWf8GydSjvb34')
        .get()
        .then((a) => hostels.push(a.data()));

    const promise4 = db
        .collection('hostels')
        .doc('qEaMhksOO3Lqus8dHw0K')
        .get()
        .then((a) => hostels.push(a.data()));

    promise1
        .then(() => promise2
            .then (() => promise3
                .then(() => promise4
                    .then)))

});
