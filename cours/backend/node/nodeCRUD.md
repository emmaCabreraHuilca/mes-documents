# Node CRUD (Get & Delete)

## Introduction 

Lorsque l'on crée un projet on peut utiliser des serveurs classiques ou des serveurs less.
- Serveurs classiques: la personne vas taper une URL et tomber sur un serveur fixe et unique
- Serveur less: il n'y a pas de serveur comme avec Node, on utilise des services (ex: azur, amazon, google clode platform)


## Node 

### Installation

Node est pré-installé sur les ordinateurs, dans le cas contraire il faut le télécharger à partir d'internet sur le site officiel Node. 
Pour pouvoir l'utiliser correctement il faut sur Webstorm activer l'auto complétion : 
file => setting => écrire node => node.js and NPM => cocher case coding assitance for node.js 


Pour vérifier si l'application est bien installé et sa version on vas dans le terminal et on tape: node -v (il faut s'assurer d'être sur le bon projet et qu'il est bien ouvert à la racine du projet).


### Applications

On vas télécharger des minis applications pour developpeur qui donnent des outils pour developper.
Pour lancer ces applications npm (gestionnaire de paquet) on vas dans le terminal et on tape : npm init (ou vérifier qu'on a le fichier package.json)


Dans package.json on a deux types d'informations les dev dependencives et les dependencives. Les dev dependencive qui contiennet les applications pour les developpeur et les dependencive qui seront utiliser par l'appli final du client.
Pour enregistrer automatiquement au bon endroit les applications que l'on vas rajouter au projet on vas écrire dans le terminal : 
- nmp i -D pour enregister dans les dev dependencives
- npm i -S pour enregister dans les dependencives


### Framework

> Framework = cadre de travail, donne des outils prêt à l'avance qui vont nous permettre de travailler

Express est le framework le plus utilisé avec Node. 
Pour le télécharger on tape dans la console : npm i -S express


Si connait pas le nom du framework, il faudra regarder sur internet comment l'installer
(cours semver => explique comment gerer les versions à venir)


## CRUD
> CRUD = Create Read Update Delete

1er fichié Node = index.js
 console.log('coucou')
 terminal = node index.js


### Routage

Express fonctionne avec un principe de routage (route) :

```js
//exemple fonctionnement routage

//je crée une constante express dans lequel je vais require/chercher le logiciel express, cela rend express disponible
const express = require('express');
// app permet de créer une application qui vas utilser la fonction express
const app = express();

//req est utilisé pour récuperer la requête qui est faite par l'utilisateur
//res est utilisé pour répondre à l'utilisateur
app.get('/', function (req, res) {
  res.send('Hello World!')
});

//crée une app qui vas écouter la racine et le port 3000 (de 0 à 75000(?), endroit disponible pour l'ordinateur d'être accessible par internet)
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})

```

Dans le moteur de rechercher on vas écrire **localHost:3000/** pour accéder à la page que l'on cherche.
LocalHost définit par écouter les serveurs sur mon ordinateurs (local : héberger par, Host : mon ordinateur)

### Nouvelle Route

Si l'on crée une nouvelle route on doit redémarer le serveur, on était la console avec **contrôle + c** et on l'a rallume avec **node index.js**.

```js
const express = require('express');
const app = express();

app.get('/', function (req, res) {
  res.send('Hello World!')
});

app.get('/number', function (req, res) {
  res.send('8')
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
```

Il existe des outils pour redémarer la console comme **livereload** qui permet de relancer la console automatiquement à chaque fois que l'on change le code.


## Demande HTTP

1. On récuperer les fonctions qu'on a créé ailleurs : importe et exporte = coder des fonctions dans un fichié et les récuperer dans un autre fichié. (à venir)
2. /hostels/:id => /:variable, paramètre que l'on vas stocker sous le nom id, comme il s'agit d'un nombre on doit le transformer en string avec parseInt( ) pour l'utiliser dans la barre URL.


### Get

On utilise le navigateur que pour les get (terminal, network)

```js
function getHostelById (id) {
    const result = hostels.find(values => values.id === id);
    if (result) {
        return result
    } else {
        return "pas d'hotel avec cette id"
    }
}

app.get('/hostel/:id', function (req, res) {
    const id = parseInt(req.params.id);
    res.send(getHostelById(id))
});
```

### Delete 

On vas utiliser postman pour delete

```js
function removeHostel (id) {
    const result = hostels.filter(values => values.id !== id);
    if (result) {
        return result
    } else {
        return "pas d'hotel avec cette id"
    }
}

app.delete('/hostel/:id', function (req, res) {
    const id = parseInt(req.params.id);
    res.send(removeHostel(id))
});
```



















