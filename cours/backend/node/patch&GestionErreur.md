# Patch & Gestion d'erreur

## Patch 

Vous remplacez qql attribtus d'un objet  

On veut juste remplacer les clés qui changent mais on ne peut pas en rajouter
            
1. on prépare la route 
ex: 
Vérifier que tout fonctionne
app.patch('/hostels/:id', function (req, res) {
    const hostelToCreate = req.body;
    const id = parseInt(req.params.id); 
    console.log(id); //voir dans la console si l'id existe
    const index = hostels.findIndex(value => value.id === id)
    
   //pour vérifier qu'on arrive à trouver l'objet que l'on veut changer
    const hostelToChange = hostels[index];
    console.log(hostelToChange)
    res.send(hostelToChange)
});               
 
Façon de faire  'supprimer les console.log qui ne servent plus à rien'
 
app.patch('/hostels/:id', function (req, res) {
    const dataToChange = req.body;
    //récupérer les clés que l'on nous envoie + console.log pour vérifer qu'elles sont bonnes
    const keysToChange = Object.keys(dataToChange);
    console.log(keysToChange);
    const id = parseInt(req.params.id); 
    const index = hostels.findIndex(value => value.id === id)
    
   //pour vérifier qu'on arrive à trouver l'objet que l'on veut changer
    const hostelToChange = hostels[index];
    //vérifer que les clés sont autorisé et qu'on peut les modifier
    const authorizedKeys = Object.keys(hostelToChange);
    console.log(authorizedKeys);
    
    const isAuthorized = authorizedKeys.find(keysToChange[0]);
    console.log(isAuthorized);
    res.send(hostelToChange);
});         
 
 
 
app.patch('/hostels/:id', function (req, res) {

    //gestion d'erreur
    try { 
        const dataToChange = req.body;
        const keysToChange = Object.keys(dataToChange);
        const id = parseInt(req.params.id); 
        const index = hostels.findIndex(value => value.id === id)
        
       
        const hostelToChange = hostels[index];
        const authorizedKeys = Object.keys(hostelToChange);
        
        
        const isAuthorized = 
            keysToChange.map(key => authorizedKeys.includes(key));
        console.log(isAuthorized); //suprimer
        
        //vérifier qu'une clé est fausse
        const isOk = isAuthorized.every(key => key);
        
        
        //gestion d'erreur
        //envoi une erreur  -> auto 500
        if (!isOk) {
            throw new Error('non authorized key');
        }
        
        console.log(isOk);
        
        res.send(hostelToChange);
    
    } catch (e) {
    
    }
 
 
});   

      
verifier si les clés sont autorisé, on vas chercher l'objet que l'on veut modifier
on vas faire la liste des clés de l'objet
- identifier l'objet => avec findIndex
            
const index => on definit un index et on vas vérifier quel objet à le même id que celui qu'on a reçu dans la demande     


## Gestion d'erreurs

    