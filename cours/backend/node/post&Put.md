# Post & Put (Codes HTTP)

- Rest : façon organiser Url 
- CRUD : opérations permettant de créer, lire, mettre à jour et  supprimer des données

## Codes HTTP

Le code http permet de déterminer le résultat d'une requête ou d'indiquer une erreur au client.
En tant que developpeur on ne ce sert pas des 100 et un peu des 200.


Les principaux codes http que l'on vas utiliser sont:

- 200
    - 200 : Ok :  tout c'est bien passé (réponse automatique avec get) 
    - 201 : Created : tout c'est bien passé + créer une ressource (à rajouter obligatoirement avec post)
- 400    
    - 400 : Bad Request : = la syntaxe de la requête est erronée (*404)
    - 401 : Unauthorized : une autorisation est nécessaire pour accéder à la ressource (*403)
    - 402 : Payment Required : paiement requis pour accéder à la ressource
    - 403 : Forbidden : comprend la requête mais refuse de l'exécuter (*401)
    - 404 : Not Found : ressource non trouvée (*400)
- 500    
    - 500 : Internal Server Error : erreur interne du serveur (le serveur à planté et je sais pas pourquoi)
    
Pour faire apparaître un code http on écrit res.status(404).send() par default il envoit automatiquement 200 = ok.

## Post 

Un post se fait à la racine de la ressource que l'on veut modifer, il n'y a pas besoin de l'id car on ne l'a connait pas à l'avance (id rajouter dans la fonction).

Il y a deux méthodes lorsque l'on crée une nouvelle ressource et qu'on doit renvoyer une réponse :
- On peut renvoyer juste l'élement qui a été crée (meilleur façon de faire)
- On peut renvoyer la liste modifié (trop de données)

### Postman 

Pour envoyer une reqûete il faut aller sur Postman :
1. Ecrire dans la barre : localHost:3000/hostels
2. Dans body on vas rajouter la requête, on sélectionne raw et dans text: json
3. Ajouter des double quote aux string, pas sur les nombres et les booleans car ils sont reconnus

### Webstorm 

Pour récupérer la requête il faut sur Webstorm : 

1. Rajouter après const express et const app :
    - app.use(express.json());
    - pp.use(express.urlncoded({extended:true}));
    
2. Ajouter **const hostelToCreate = req.body**, le body sera conserver dans req   

3. Faire un console.log de creativedHostel pour voir si ce qu'on récupère est utilisable ou pas 
    - si undefined : vérifier les double quote 
    - ne pas oublier de send (webstorm converti automatiquement en .js le .json)

```js
app.use(express.json());
pp.use(express.urlncoded({extended:true}));


function addHostel (hostel) {
    const buffer = {...hostel};
    
    buffer.id = hostels.length + 1;
    hostels.push(buffer);
    
    console.log(hostels);
    return buffer;
}     


app.post('/hostels', function (req, res) {
    const hostelToCreate = req.body;
    const createdHostel = addHostel(hostelToCreate);
    res.status(201).send(createdHostel);
})    
```            

## Put

Put consiste à modifier un objet qui existe déjà, on put sur un objet particulier

### Postman 

Pour envoyer une requête il faut aller sur Postman :
1. Ecrire dans la barre : localHost:3000/hostels/id (!!!!!)
2. Dans body on vas rajouter la requête, on sélectionne raw et dans text: json
3. Ajouter des double quote aux string, pas sur les nombres et les booleans car ils sont reconnus

### Webstorm

On a besoin de l'id, on choisie l'id de l'objet que l'on veut modifier   

```js
app.use(express.json());
pp.use(express.urlncoded({extended:true}));

function putHostel (id, newHostel) {
    const index = hostels.findIndex(value => value.id === id);
    hostels[index] = x;
    return newHostel;
}
console.log(putHostel(3, newHostel));

            
app.put('/hostels/:id', function (req, res) {

    const hostelToCreated = req.body;
    const id = req.params.id;
 
    const hostelCreated = putHostel (id, hostelToCreated);
    res.send(hostelCreated)
});    
```
            