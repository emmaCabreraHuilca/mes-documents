# Boucle For

Une boucle for est un outils qui nous permet de faire des boucles.
Il est similaire à map et à forEach sauf que ces operators vont boucler sur un tableau, la boucle for c'est nous qui allons décider que quoi elle va boucler.

## Description

Les boucle for se divisent en deux élèments :
- for ( ) : partie conditionnel : sert a exprimer les conditions dans laquel les boucle for vont tourner.
- { } : partie éxecuter : sert à dire ce qu'il y a dans la coucle for et qui est l'équivalent de la fonction callback qu'on a dans forEach ou dans un map.

## Fonctionnement

La partie éxecuter est composé de trois parties :
```javascript
for ( 'initialisation' ; 'prédicat/booléan' ; 'itération') {
}
```
Ces trois parties sont optionnels, on peut faire une boucle for sans une de ces trois parties.

```javascript
const names = ['Dupont', 'Durand', 'Bonnet'];

for (let i = 0; i < name.length; i++ ) {
    const name = names[i];
} 
```

### Initialisation 

L'initialisation est la partie ou l'expression de code vas être évalué au démarrage de la boucle for.
Dans cette partie du code vas être éxecuté une fois, on peut très bien commencer la boucle for sans initialisation.

Si let i = 0 => commence à zéro

### Prédicat / Booléan

Le prédicat est la partie qui sert à savoir dans quels conditions ont vas continuer à jouer la boucle for.
Elle vas renvoyer un prédicat (il n'y a pas de code éxécuté), il ne faut pas écrire true ou false :
- si true la boucle for vas continuer à se jouer a l'infini
- si false la boucle for ne je jouera jamais

On peut rajouter un prédicat dont la condition peut avoir aucun rapport avec ce qu'on fait dans la boucle for.
Il faut au moins une condition pour que la boucle continu.

Exemple : i < name.length ; i < 10

### Itération

L'itération est la partie ou un bout de code sera éxecuté à chaque fois qu'on fini la boucle for, il peut y avoir n'importe quel code.
> i++ est l'abréviation de i = i + 1 

Les diviser et multiplier ne servent à rien :
- i-- => i = i - 1 
- i+=2 => i = i + 2 
  

## Utilisation

Le but de boucle for est de générer du contenu :
```javascript
function randomize(start, end) {
    return Math.floor((Math.random() * end) + start)
}

const names = ['Dupont', 'Durand', 'Bonnet'];

for (let i = 0; i < name.length; i ++ ) {
  console.log(names[i])
} 
```
On peut l'utiliser pour trier des nombres pair consécutif ou pour créer un tableau d'utilisateur test.
On vas pouvoir générer des appels à la base de donnée dans une boucle for et donc de construire des bases de données.

### Astuce

On écrit .length pour ne pas obtenir de undefined dans les résultats.

Si je veux afficher un nom/nombre sur deux j'utilise l'itération : i+=2.

On utilise modulo pour savoir si un nombre est divisible par un nombre ou pas:
- si 0 est divisible par ce nombre
- si + de 0 n'est pas divisible par ce nombre

### Exemple

En faisant varier les paramètres on peut faire faire ce qu'on veut à la boucle for.
 
On peut générer des users :
```javascript
function randomize(start, end) {
    return Math.floor((Math.random() * end) + start)
}

const users = [];

const lastNames = ['Dupont', 'Durand', 'Bonnet'];
const firstNames = ['A', 'B', 'C'];

for (let i = 0; i < 10; i++ ) {
  users.push({
    lastname : lastNames[randomize(0, 3)],
    firstname : firstNames[randomize(0, 3)],
    age : randomize(0, 99),
    zip : randomize(1000, 95880)
  })
} 

console.log(users)
```
