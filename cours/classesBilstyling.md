# Classes Bilstyling 

## 

exemple => classes mammifère => humain, souris, chat, ect
élèments en commun => yeux, bouches, ect
commmun mais différents => marches sur deux pieds ou sur quatres

en version informatiques :

classes principal => mammifère
sous classes  => 

certaines choses ne marches quand typescript 
reprend tssrc => 



## Premier système

### index.ts

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get('/', function (req, res){
    const myHotel = new HotelClass(12, 'name hotel des fleurs');
    console.log(myHotel.id);
    res.send(myHotel);

});


### hotel.model.ts

? => toujours derniers arguments !!!!


import {RoomModel} from "./room.model";

export interface HotelModel {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms?: RoomModel[];
    isValidated? : boolean;
}

export class HotelClass {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated? : boolean;

    constructor(
        id: number,
        name?: string,
        roomNumbers?: number,
        pool?: boolean,
        isValidated?: boolean,
    ) {
       this.id = id;
       this.name = name;
       this.roomNumbers = roomNumbers;
       this.pool = pool;
       this.isValidated = isValidated;
    }

}


## Deuxième système 

### index.ts

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get('/', function (req, res){
    const myHotel = new HotelClass({id: 12, name: 'hotel des fleurs'});
    console.log(myHotel.id);
    res.send(myHotel);

});



### hotel.model.ts

Object assign => prend propriété objet a et les insérer dans objet b


import {RoomModel} from "./room.model";

export interface HotelModel {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    rooms?: RoomModel[];
    isValidated? : boolean;
}

export class HotelClass {
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated?: boolean;

    constructor(hotel: HotelModel) {
    Object.assign(this, hotel)
    }
}


## Méthode 

class donner méthode/ fonction spéciael qui vont s'appliqur aux instanc et manipuler les objets

ex:  

### index.ts

app.get('/', function (req, res){
    const myHotel = new LuxHotelClass({
        id: 12,
        name: 'hotel des fleurs',
        rooms: [],
//A la suite         
        numberOfRoofTops: 2,
        numberOfStars: 4});
//        
    myHotel
        .calculateRoomNumbers()
        .createPool();
    res.send(myHotel);

});



### hotel.model.ts

import {RoomModel} from "./room.model";

export interface HotelModel {
    rooms: RoomModel[];
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated? : boolean;
}

export class HotelClass {
    rooms: RoomModel[];
    id?: number;
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    isValidated?: boolean;


    constructor(hotel: HotelModel) {
        this.rooms = hotel.rooms;
        Object.assign(this, hotel)
    }

    calculateRoomNumbers(): HotelClass {
        this.roomNumbers = this.rooms.length;
        return this;
    }
    
    destroyPool(): HotelClass {
        this.pool = false;
        return this;
    }

    createPool(): HotelClass {
        this.pool = true;
        return this;
    }
}

// A la suite 

export interface LuxHotelModel extends HotelModel {
    numberOfStars: number;
    numberOfRoofTops: number;
}

export class LuxHotelClass extends HotelClass{
    numberOfStars: number;
    numberOfRoofTops: number;

    constructor(hotel: LuxHotelModel) {
        super(hotel);
        this.numberOfStars = hotel.numberOfStars || 1;
        this.numberOfRoofTops = hotel.numberOfRoofTops || 1;

    }

}


## Autre exemple 

### index.ts

app.get('/', function (req, res){
    const myRoom = new RoomClass(12)
        res.send(myRoom);

});

### room.model.ts

export interface RoomModel {
    roomName: string;
    size: number;
    id: number;
}

export class RoomClass implements RoomModel{
    roomName: string;
    size: number;
    id: number;

    constructor(id: number, roomName: string = '', size: number = 0) {
        this.roomName = roomName;
        this.size = size;
        this.id = id;
    }

}