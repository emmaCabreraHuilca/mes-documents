# Falsy

exemple: prédicat

----

const a = 5;
if (a < 10) {
    console.log('ok')
}
// se joue

const a = 15;
if (a < 10) {
    console.log('ok')
}
// ne se joue pas 

----

const a = 15;
if (false) {
    console.log('ok')
}
// juste mais ne se jourra jamais

const a = 15;
if (true) {
    console.log('ok')
}
// juste mais se jourra toujours

----

