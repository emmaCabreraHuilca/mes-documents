# Angular 

Angular permet de découper les choses en modules qui ont un thème commun et de faire des components qui doivent être utilisé au maximum.

Certains élèments reviennent régulièrement, ils sont présent sur toutes les pages d'un même site.
Pour ne pas avoir à les re-coder sur chaques pages ont vas utiliser des modules et des components d'angular. 
- Module : regrouper tout les contenus qui traîtent un même thème ensemble 
(exemple: publicité, )
- Component : un bout d'interface de l'application. 
Il charge ses propres données et s'auto-initialise, on peut le prendre et le mettre n'importe ou dans notre application.
(exemple: publicité carré ou en longueur)

## Introduction

Sur une page internet la structure principal reste la même, seul le contenu change d'une page à l'autre.
La page est composé de modules qui contiennent plusieurs components qui vont se répéter.

Pour faire fonctionner les components ont peut leur faire passer des entrées et des sorties.
On vas pouvoir avoir un mécanisme ou on peut avoir un component père et des components enfants.
Le components père peut faire passer des informations aux enfants comme refresh les pubs au bout de tel temps.


Il est important de retenir qu'il ne sert à rien de re-coder la même chose sur chaques pages, il ne faut pas avoir de duplicate content.

exemple CRUD : au lieu d'avoir plusieurs CRUD il faut faire un component et y mettre les règles pour pouvoir le reprendre sur chaque page, seul l'url change.


=> Suite 

## Installation 

- Installer => angular cli => récupérer le premier lien : terminal => nmp install -g @angular/cli => Y

> cli : command line interface (permet de donner des ordres a angular) 

- rajouter au projet :
- créer un dossier front 
- terminal => aller au dossier front : cd front  (retourner en arrière : cd ..)
- ng --version pour tester que l'installation a bien fonctionner
- ng new myAngular (new nv projet)
- Y => SCSS
- git + add les nv fichiers

- package.json => clique droit + show npm scripts 
- start 

=> 








       