# Array & Operators


## JavaScript

On peut faire du javascript à partir de n'importe quelle page HTML.

### Pour commencer

1. Dans console.log('coucou') pour vérifier si le fichier JV fonctionne.
2. Rajouter dans fichier html avant le **/body** : 

```html
<script src= fichie.js> </script> 
```
## Array

Un array est un tableau dans lequel on peut metttre n'importe quoi. 
Pour pouvoir l'utiliser il faut qu'il y est le même genre de choses dedans et que les éléments existants.
```js
const tab = [];
```

### Eléments

On peut connaître la longeur d'un tableau, soit combien d'éléments il possède avec la commande:
```js
console.log (tab.length);
```
Le premier élément du tableau a l'index 0 (on commence à compter à partir de 0 en informatique).

Pour faire apparaître un élément du tableau on utilise :
```js
//Elément 3
console.log(tab[2])
```
Pour faire apparaître le premier élément on écrit 0 et pour faire apparaître le dernier élément on écrit (tab.length - 1)


 
## Operators
 
Pour pouvoir faire des opérations dans un tableau on utilise un  operator qui sera toujours situé à la fin du tableau.

Les operators ont tous le même mode de fonctionnement, ils commencent dans un tableau, ils parcourent le tableau 1 par 1 jusqu'à la fin du tableau en applicant une fonction.
Le seul qui ne parcourt pas tout le tableau c'est some parcequ'il s'arrête dès qu'il trouve ce qu'il cherche.


### ForEach

Il sert à appliquer une fonction à un élément que l'on parcourt (pour chaque élément du tableau tu vas faire quelque chose).
```js
//affiche age + 10
tab.forEach(element => console.log(element.age + 10));

//affiche age + 2
tab.forEach(element => console.log(twice(element.age)));
function twice (x) {
return x + 2 }
```
 
### Map

Il renvoie à quelque chose et permet de modifier la façon dont fonctionne l'objet, il prend un objet du tableau et le transforme en un objet dans un autre tableau.
 
#### Transformation simple 

Je récupère un petit élément du tableau et je vais le modifier.

```js
//Fabrique un nv tab ou il n'y aura que les ages
const tabAge = tab.map(element => element.age);
console.log(tabAge);

//Fabrique un nv tab ou il n'y aura que les ages * 10
const tabAgeTimes10 = tab.map(element => element.age * 10);
console.log (tabAgeTimes10)
``` 

### Filter

Il permet de filtrer les éléments d'un tableau 

```js
const moinsde40 = tab.filter (element => element.age <40);
console.log (moinsde40);
```

### Sort

Il permet de ranger les éléments d'un tableau dans un ordre croissant ou décroissant.

```js
//ordre croissant
const orderedTab = tab.sort ((a, b) => a.age - b.age);
console.log (orderedTab);

//ordre décroissant
const orderedTab = tab.sort ((a, b) => b.age - a.age);
console.log (orderedTab);

//exemple trier par nom 
const tabByName = classe.sort ((a, b) => {
    return a.roomName < b.rommName ? -1 : 1
});
```

### Every

Il permet de savoir si tout les éléments du tableau répondent à un prédicat (True or False)

```js
const areAllMajor = tab.every (element.age > 18);
console.log(areAllMajors);
```

### Some

Il permet de savoir si au moins un des éléments du tableau répond à la demande (True)

```js
const areAllMajor = tab.some (element.age > 18);
console.log(areAllMajors);
```

### Push

Il permet de rajouter un élèment dans le tableau

```js
const newPerson = {firstName: nom, age: 18};
class.push(newPerson); 
console.log(classe);
```


### Reduce

Reduce renvoit quelque chose mais on ne sait pas quoi à l'avance, c'est nous qui allons décider de ce qu'il vas renvoyer.
> reduce((previousValue, currentValue) => function/opération, valeur de départ)

La valeur de sorti est de la même nature que la valeur de départ. 
Si la valeur de départ est un string, nous aurons un string en valeur d'arrivé, pareil si il s'agit d'un nombre, ou un array...

#### Fonctionnement

- previous value, current value => opération entre previousValue et currentValue, le previousValue s'initialise avec la valeur de départ.
- Le résultat de l'opération vas devenir le nouveau previousValue, puis on vas rentrer dans la boucle et refaire l'opération avec le currentValue.

Lorsqu'on utilise reduce on commence au début du tableau et on le parcourt jusqu'à la fin on ne peut l'arrêter à un moment donné (pas de condition de sortie).
Le résultat final vas dépendre de l'opération (return previousValue + currentValue).
    
```js
// La valeur de départ : ["ab", "cd", "ef", ]

reduce((previoous, current) => { 
    return previous + current 
    }, " ") 

//La valeur d'arrivé : "abcdef"
```
