# Function

On déclare une constante avec les variables (var) let et const.

## Fonction
 
Une fonction (function) se déclare comme une variable, avec un nom, mais il faut changer quelques élèments:
> function nom(paramètre) { dans laquel la fonction vas s'éxecuter }

Les fonctions permet de déclencher des ordres et peuvent renvoyer quelques choses ou rien (=void)
```js
function coucou1() { 
console.log('coucou')
};
//cette fonction ne renvoie rien

function coucou(){
return 42}

const exemple = coucou();
console.log(exemple) 
//renvoie 42
```

### Fonction interactive

Pour qu'une fonction soit interactive on rajoute des paramètres de fonction:  
```js
//exemple 1:
function produit(x, y) {
return x * y
};
//(x:2, y:10)
console.log(produit(2, 10));
//20

//exemple 2:
function randomAction (x, y) {
if (x < 10) {
return x * y };
if (y > 20) {
return x - y }
}
//(x:2, y:10)
console.log(randomAction(2, 10))
```

Les parenthèses lorsqu'on appelle une fonction`sont obligatoire, ce que l'on veut obtenir est égal à l'exetution de la fonction.

### Return 
 
Lorsqu'il y un return, la fonction arrête de s'executer, il met fin à la fonction.

Il faut s'assurer qu'il est toujours une valeur de retour et que ce soit cohérent (retourne un nombre, un string ou autre), si il s'agit d'une fonction qui renvoit quelque chose.

## Rest Operator

Il n'y a qu'un **rest operator** dans une fonction et il se place toujours en dernier.
On peut mettre autant d'arguments que l'on veut avec le rest operator, la norme est cependant de 4: 

> function randomAction (x, y, a, b , c) { }

### ...arg ou z

On l'utilise quand on ne sait pas combien il y aura de paramètre: function another (...arg ou z) { }

```js
function randomAction (...z) {
    console.log(z);
    z.map(value => console.log(value))
}

//prend tout les arguments du z et les met dans un tableau
console.log(randomAction(20, 10))
``` 

#### Produit avec arguments randoms

On peut faire une fonction qui vas faire le produit de quelque soit le nombre d'argument que je vais mettre :

```js
function ramdomAction (...z) { 
return z.reduce((previousValue, currentValue) => previousValue * currentValue)
}
//(z: 20, 10, 5)
console.log(randomAction(20, 10, 5));
//1000
```

#### Paramètre classique

On peut mixer le **rest operator** avec des paramètres classique :

```js
function ramdomAction(a, b, c, ...z){
console.log(a + b +c, z)
}
//(a:1, b:2, c:3, z:10)
ramdomAction(1, 20, 3, 10)
//24 [10] 
```

## Enchaîner les Operators

On peut enchainer les operators, on appel cela la programmation fonctionnel, on a des fonctions qui appels des fonctions.

```js
//sans fonction

const result1 = hostels
    .sort((hostel1, hostel2) => hostel1.roomNumbers - hostel2.roomNumbers);
console.log(result1);

//avec fonction

const result = hostels
    .sort(sortHostelByRoomNumbers)
    .filter(filterRoomNumbers);

function sortHostelsByRoomNumbers (hostel1, hostel2) { 
return hostel1.roomNumbers - hostel2.roomNumbers
}

function filterRoomNumbers(hostel) {
return hostel.roomNumbers > 3
}

console.log(result)

//on obtient les hôtels trier par nombre de chambres (ordre croissant) et seuls les chambres avec plus de 3 chambres apparaissent.
```







