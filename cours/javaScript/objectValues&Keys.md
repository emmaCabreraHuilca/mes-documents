# Object Values & Object Keys

Il est utile de transformer un objet en tableau temporairment pour pouvoir le parcourir.
On peut ainsi changer les keys et values de l'objet avec la tableau.

La transformation d'un objet en tableau ne permet pas de tout transformer en tableau, on vas soit récupérer les keys soit les values.


## Object.keys

On va prendre l'objet "my" et chercher toutes les keys d'un objet et les mettres dans un tableau : Object.keys( );

### Exemple

On veux changer les keys de l'objet "my" en les mettant en majuscule et changer les valeurs 0 (falsy) par 10.

```javascript
const my = {
    tata : 7,
    toto : 1,
    tutu : 5,
    titi : 2,
};

const up = Object.keys(my).map(key => key.toUpperCase());
console.log(up); 
//Met toutes les id en majuscule


Object.keys(my).forEach(key => {
    const upperKey = key.toUpperCase();
    my[upperKey] = my[key] + 10;
//Pour supprimer un élèment de l'objet ont utilise l'operator delete.
    delete my[key];
});
console.log(my)

```

Pour accéder à la propriété d'un objet il existe deux façon de faire :
const value = 'tata';
- my[value] : quand le nom de la clé vient par une variable, on vas utiliser les crochets pour dire que l'on veut la clé tata qui est stockée dans la value "value".
- my.tata : quand on connait déjà le nom de la clé que l'on veut modifier.

## Object.values

On va prendre l'objet "my" et chercher toutes les values d'un objet et les mettres dans un tableau : Object.values( );

### Exemple
 
On veut parcourir les valeurs de l'objet "my" et les regrouper pour en faire un seul mot  

```javascript
const my = {
    tata : 'a',
    toto : 'b',
    tutu : 'c',
    titi : 'd',
};

const sentence = Object.values(my).reduce((previousValue, currentValue) => previousValue + currentValue);
console.log(sentence)
//'abcd'
```



## Ne pas faire 

```javascript
my.tata = 10;
my.TATA = my.tata;
delete my.tata;
console.log(my)
// met deux key tata 
```


