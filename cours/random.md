# Random 

Les nombres aléatoires n'existent pas en informatique, il s'agit de pseudo-randomization.

Lorsque l'on fait appel à la commande random pour donner un nombre aléatoire celui-ci sera basé sur l'horloge de l'ordinateur que l'on utilise.
Ce nombre n'est donc pas aléatoire car on peut le déterminer en connaissant l'heure à laquel on a utilisé l'outil random, ce qui pose des problèmes en cryptologie.

Lors d'un entretient il est recommandé de parler de nombres pseudo-aléatoires parce que sur un ordinateur les nombres aléatoires sont générés à partir de facteurs connu ce qui n'est pas aléatoire.


## Math

On fait appel à la bibliothèque Math qui est fourni par JavaScript et qui permet de faire des opéarations mathématiques.
> Il ne faut pas utiliser JavaScript ou Node pour réaliser des opérations mathématiques.

Si on veut réaliser une opération avec une virgule, on doit rajouter * 100 & / 100 pour éviter les bugs.
```javascript
console.log((0.1 * 100 + 0.2 * 100) / 100)
```

Lorsqu'on utilise un langage informatique il faut en connaître ses défauts.

## Utiliser random

Avec random on obtient un chiffre inférieur à 1 :
```javascript
console.log(Math.random())
```

Si je veux un chiffre aléatoire entre 1 et 10 ou 100: 
```javascript
console.log(Math.floor((Math.random() * 10) + 1));
console.log(Math.floor((Math.random() * 100) + 1));
```

Autre exexmple :
```javascript
function randomize(start, end) {
    return Math.floor((Math.random() * end) + start)
}

const names = ['Dupont', 'Durand', 'Bonnet'];

console.log(names[randomize(0, 3)]);
```















