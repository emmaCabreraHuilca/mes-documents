# Cours Bootstrap

## Structurer une page HTML

### Architecturer un site internet statique

Il faut définir le nombre de page pour son site 

Il faut créer un dossier par page :
- Home Page (page principe)
- Assets (stockés images et logos)
- Vendor (script ou autre qui ne provient pas de nous)

Exemple : FAQ, About, ...

#### Important

Avant de s'interésser au CSS il faut donner les informations importantes et structurer le contenu du site.


### HTML

#### Sur Webstorm

Ajouter les nouveaux documents dans **git** pour pouvoir **push**


#### Balises pour le réferencement

Ces balises permettent à Google de comprendre que le site est séparé en plusieurs parties.

##### Section

Permet de diviser la page en plusieurs parties

##### Header 

Introduction de la section, là où il vas y avoir le titre et le texte d'introduction

##### Footer 

Le **footer** sert à ajouter des informations supplémentaires en fin de section (ex: date de création de la page)

> Les balises **header** et **footer** peuvent être remplacés par des **div**. 


```html
<section>
    <header>

        <h2> Titre </h2>

    </header>

    <h3> Troisième titre </h3>
    <p> contenu de la section, pas dans header ou dans footer </p>

    <footer>

    </footer>

</section>
```

## Bootstrap 

### Définition

Bootstrap est un **framework** qui permet de structurer le contenu d'une page HTML avec des outils fait par d'autres gens et de ne pas utiliser du CSS.

Pour commencer il faut rajouter dans <header> les liens suivants :

```html
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>`
``` 

### Component

A venir

## Vocabulaire

### Navigateur 

Un **navigateur** est un parser qui vas parcourir des fichiers HTML et qui en fonction de ce qu'il va lire vas déclencher des actions.