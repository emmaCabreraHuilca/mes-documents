# Personnaliser un fichié Bootstrap


## Version Bootstrap 

On peut directement personnaliser Boostrap,dans Bootstrap theme on modifie les paramamètres bootstrap avec du Sass. 

Ce n'est pas recommandé car il faut au moins être spécialisé en intégration et en UX. 

## Version simple

1. Dans assets créer un nouveau dossier styles 
2. Créer un fichié bootstrap.css
3. Rajouter dans header après les liens bootstrap pour que les informations soient prises en compte en priorité


### Dans le fichié bootstrap 

On vas à la place surcharger c'est à dire prendre la classe d'origine et modifier par dessus 
1. Sélectionner les modifications à apporter puis les coller dans le 
fichié
2. Modifier les élèments bootstrap (doucle click couleurs a gauche du code css)
3. Regarder dans la console puis sélectionner :hov pour mettre en hover 


## Version Autre

On peut créer un style perso qui n'est pas prévu par bootstrap , on vas créer un style dédié avec un fichié otherStyles.css et y rajouter des élèments des styles qui ne sont pas proposer par Bootstrap.

exemple: super-flicker.css


### Organiser les feuilles de styles

On vas ajouter dans les fichés bootstrap.css, otherStyles.css et allStyles.css : @import nom du fichié pour regrouper tout les fichiés css dans un seul fichié.
- Dans le dossier style on vas créer un dossier myBootstrap et y mettre les fichiés bootstrap.css que l'on a modifié.
- On vas créer un dossier otherStyles et y ajouter tout les fichiés styles.css que l'on a créé.
- On vas également créer un fichié allStyles.css et y importer les deux autres dossiers = @import myBootstrap, @import otherStyles

Pour éviter une perte de performance on évite de rajouter tout les fichiés styles à chaque page de notre site.
           






