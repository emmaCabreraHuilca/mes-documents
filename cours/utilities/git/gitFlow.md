# Merges request

## Introduction 

Dans un projet on a une Branch principal appelé master, c'est la Branch de référence/branche principale du site
c'est elle que l'on push en CDN.

Lorsque l'on travail à plusieurs, chacun vas modifier et apporter des élèmetens au projet, ils vont créer plusieurs branches. 
Le chef de projet vas recevoir toute les versions qui ont été faites et vas les ajouter à la branche master. 
 
On vas faire les branches d'un projet en fonction de ce qu'on fait, de la fonctionnalité sur laquelle on travaille, chacun se voit attribuer une tache :
 - feature/newTitleFAQ
 - feature/newCarouselFAQ 
 - feature/ detailsProfil
 
> feature : une fonctionnalité 
 
## Créer une nouvelle branch 
  
Pour créer une nouvelle branche il faut : 
 1. Sélectionner **Git: master** en bas à droite de l'écran
 2. Séléctionner **+ New Branch** 
 3. Donner un nom à la branch (ex: features/detailProfilForFAQ)
 
On peut commit et push sur gitlab les mofications apporter par la personne, on peut accéder aux modifications des branches sur gitlab.
Il faut penser à récuperer les élèments rajoutés sur la branch master. 

Pour passer d'une branche à une autre il faut séléctionner la branch que l'on veut ouvrir et sélectionner checkout.
Il faut également sauvergarder les modifications apportés pour pouvoir chekout et changer de branch.


## Merger les branches à la master 

Il y a deux façons de merge sur la branch master :


### Version Non Officiel 

1. Dans la branch master, on vas sélectionner **VCS** puis **merge changes**
2. Faire un commit et un push des branches que l'on veut ajouter à la master
3. On vas supprimer les branches qui ont été ajouté à la master et à gitlab

Il peut y avoir des conflits dans le cas où deux personnes essayent de modifier le même élèment au même endroit.
Dans ce cas là, une fenêtre de résolution de conflit vas s'ouvrir. 
Il faudra choisir les modifications que l'on veut apporter à la master et supprimer les autres.


### Version Officiel  

La version officiel demande que le merge request soit valider par une tierce personne ou par le chef de projet. 

1. Faire un commit et un push 
2. Sur gitlab, on vas sélectionner **merge requests** puis **create merge request**  
3. Cliquer sur **change branches**, on vas pouvoir choisir sur quelle branche on vas faire le merge request 
4. Décrire les modification apportés et assigner à une personne pour qu'elle puisse valider ou pas la demande
5. Soumettre merge request

La personne à qui a été assigné le projet vas recevoir une notification. 
Elle vas pouvoir voir les modications apportés au projet dans **change** et les **commits** de la branch.
Pour valider la demande la personne doit sélectionner **merge** et **delete source branch**.
 
Lorsque plusieurs personnes travaillent sur un même projet il peut arriver qu'elles fassent des merges en même temps.
On vas avoir des conflits et ce n'est pas a chef de projet de régler le problème.
Il vas merger une nouvelle branche et vas la signaler aux personnes qui ont des merges en attente. 
Ils vont récupérer les changements fait par les autres et résoudre les conflits eux même et faire une merge request à la fin.
Le chef de projet vas ensuite merge et commit le tout sur la branch master.

## Annuler une merge request 

On peut annuler les mofications apporer au projet, il faut sélectionner **revert** en haut à droite de l'écran.
 
 
 
 
 

 
 
 
