# Gitignore

## Fichié Caché 

### Fonction

Certains fichiés ne sont pas visibles, ils contiennent des informations tel que la manière dont fonctionne **Git**, les fichiers que l'on a **commit**, ect.

Si un fichié caché est supprimé tout les élèments qu'il contient seront supprimés également. 

Ils ne s'envoient pas quand ont les **commit** ou les **push**.


### Sur Webstorm 

On peut visionner les documents Webstorm par **Project Files** pour voir tout les fichiés existants sur le projet.

Les fichiés cachés ont un . devant leur nom (ex: .idea). 
> **.idea** est un fichié de configuration du navigateur Webstorm, il contient des informations tel que la couleur des îcones ou la taille des fenêtres.

Les documents cachés ont une couleur vert foncé pour les différencier des documents visibles.


## Gitignore

### Fonction

Avant même de commencer à coder il faut créer un fichié **.gitignore** pour être sur qu'à l'avenir on ne risque pas de polluer le ripo avec des élèments dont on a pas besoin.
- Les documents rajouter à **.gitignore** ne seront pas pris en compte par **commit** et **push**.

### Sur Webstorm

En fonction de la configuration de Webstorm le document gitignore sera crée automatiquement.

Si ce n'est pas la cas il faut :
1. créer un nouveau document .gitignore 
2. l'ajouter dans git
 
Par la suite on pourra rajouter des documents pour qu'ils n'apparaissent plus.

### Git Add

Si Webflow ne prend pas en compte le fichié **.gitignore** il faudra forcer nous même pour que le fichié soit retirer du git.

Dans la console du terminale il faudra rajouter:
- git rm "nom fu fichié" -f

## ReadMe  

**ReadMe.md** est un fichié **markdown** qui est automatiquement crée lorsque l'on utilise du **git**.

Il sert à presenter notre projet, à le décrire, il donne des informations sur comment lancer le projet ou comment il fonctionne.

On pourra égelement y mettre les liens vers des projects privée **GitLab**.


## Push et Commit 

Il est important de **commit** et **push** régulièrement, pour pouvoir voir à chaque moments les changements apportés aux fichiés.
- permet de ne pas perdre son travail
- permet de **pull** si nécessaire

### Sur Gitlab 

On peut visionner tout les **commit** qui ont été fait, on peut voir la liste des **push** minutes par minutes. 

Lorqu'il y a des erreurs on peut revenir sur le **commit** et l'annuler, on enlève ainsi les élèments qui ne vont pas.

Plus petit sont les **commits** plus le contrôle que l'on a sur le projet est important.




