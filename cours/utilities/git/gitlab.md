# Gitlab 

Gitlab comme Github sont des sites internet qui utilisent le protocole git. 
> Git est un protocole qui permet de stocker des informations, du contenu informatique ou autre.

## Faire un nouveau projet Gitlab 

1. Sélectionner **+ New Project**
2.  Dans **Blank Project**
    - Ecrire un nom dans **Project Name** (ex: _Mon Document_)
    - Cocher dans **Visibility Level** -> Public & README
3. Valider en cliquant sur **Create project**


## Stocker un document de Gitlab à Webstorm

### Sur Gitlab
1. Dans _Mon Document_  sélectionner **clone**
2. Copier le lien **HTTPS**

### Sur Webstorm 

1. Sur la page d'acceuil cliquer sur **Check out from Version Control**
   - Sélectionner **Git**
   - Coller le lien dans **URL**
   - Valider en cliquant sur *Clone* 

2. Cliquer sur **VCS** ou sur l'icone "valide" à droite de l'écran 
   > Commit permet de sauvegarder le code sur _l'ordinateur_.
   - Sélectionner **Commit** 
   - Ecrire les modifications apportés dans **Commit Message**
   - Décocher **Perform code analysis** & **Check TODO (Show All)**
   - Valider en cliquant sur **Commit**
  
   
3. Cliquer sur **VCS** ou sur l'icone "flèche décroissante" à droite de l'écran
   > Push permet de sauvegarder le code sur le _site internet_.
   - Sélectionner **Git**
   - Sélectionner **Push**
   - Valider en cliquant sur **Push**
   
     
## Vocabulaire

### wysiwyg 

Définition : "What you see is what you get" permet à l'utilisateur de voir son document tel qu'il sera publié.

  
   
      
    







