# Cours Markdown
 
 
## Définition 

Le **Markdown** est un langage de prise de note et de mise en forme de texte.
 
Il est très important pour les développeur, il permet d'être organisé et ordonné, il est simple et rapide à utiliser.


Il est utilisé sur de nombreux sites différents (ex: Slack, gitlab,...), la manière de l'utiliser peut changer d'un site à l'autre.


## Structurer une page 

Il faut diviser la page en plusieurs parties et sous-parties.
L'utilisation de titre de différents niveaux en **HTML** et en **Markdown** permet d'obtenir une structure ordonnée.

Il faut construire sa page de manière sémantique et non de manière esthétique pour qu'elle soit reconnu par les moteurs de recherches et bien référencé.
> SEO (Search Engine Optimisation) techniques mises en oeuvres pour améliorer la position d'un site web dans les résultats des moteurs de recherche.


## Exemples de Markdown

| Markdown | HTML | Utilisation |
|----------|------|-------------|
| #        | h1   | Titre de la page, s'utilise qu'une fois dans la page |
| ##       | h2   | Titre Secondaire permet de structurer le texte, plusieurs par page |
| ** **    | bold | Texte en gras |
| _ _      |italic| Texte en italique |
| ~~ ~~    |strike| Texte barré |
| ```     |backtick| Ajouter du code dans la page |

